ABSOLUTE_PATH=`pwd`
BUILD_FOLDER=$ABSOLUTE_PATH/build_folder
SRC_FOLDER=$ABSOLUTE_PATH/src
INSTALL_FOLDER=$ABSOLUTE_PATH/install_folder

INC_FOLDER=$INSTALL_FOLDER/include
LIB_FOLDER=$INSTALL_FOLDER/lib
PKG_FOLDER=$INSTALL_FOLDER/pkgconfig

# Libraries folder
SRC_PKG_CONFIG=$SRC_FOLDER/pkg-config-0.29.2
SRC_ZLIB=$SRC_FOLDER/zlib-1.2.11
SRC_JPEG=$SRC_FOLDER/jpeg-9b
SRC_XZ=$SRC_FOLDER/xz-5.2.3
SRC_JBIG=$SRC_FOLDER/jbigkit-2.1
SRC_TIFF=$SRC_FOLDER/tiff-4.0.8
SRC_LCMS=$SRC_FOLDER/lcms2-2.8
SRC_PNG=$SRC_FOLDER/libpng-1.6.29
SRC_OPENJPEG2=$SRC_FOLDER/openjpeg-2.1.2
SRC_IM=$SRC_FOLDER/ImageMagick-7.0.7-0

function clean_build_folder {
	rm -r $BUILD_FOLDER 2>/dev/null
	mkdir $BUILD_FOLDER
	cd $BUILD_FOLDER
}

# Clean build folder
clean_build_folder

# Build zlib
cmake $SRC_ZLIB -DCMAKE_INSTALL_PREFIX=$INSTALL_FOLDER -DCMAKE_BUILD_TYPE=release
make -j8 install
clean_build_folder

# build pkg-config
$SRC_PKG_CONFIG/configure --prefix=$INSTALL_FOLDER --with-internal-glib
make -j8 install
clean_build_folder

# Build jpeg
$SRC_JPEG/configure --prefix=$INSTALL_FOLDER
make -j8 install
clean_build_folder

# Build xz
$SRC_XZ/configure --prefix=$INSTALL_FOLDER --disable-rpath --disable-lzma-links --disable-scripts --disable-lzmainfo --disable-lzmadec --disable-xzdec --disable-xz
make -j8 install
clean_build_folder

# Build jbig
cp -a $SRC_JBIG/ ./
make -j8
cd libjbig
cp libjbig.a $LIB_FOLDER
cp libjbig85.a $LIB_FOLDER
cp jbig.h $INC_FOLDER
cp jbig_ar.h $INC_FOLDER
cp jbig85.h $INC_FOLDER
clean_build_folder

# Build libpng
$SRC_PNG/configure --prefix=$INSTALL_FOLDER --enable-intel-sse=yes
make -j8 install
clean_build_folder

# Build libtiff
cmake $SRC_TIFF -DCMAKE_INSTALL_PREFIX=$INSTALL_FOLDER -DCMAKE_BUILD_TYPE=release -DBUILD_SHARED_LIBS=yes \
                -DJPEG_INCLUDE_DIR=$INC_FOLDER -DJPEG_LIBRARIES=$LIB_FOLDER \
                -DJPEG_INCLUDE_DIR=$INC_FOLDER -DJPEG_LIBRARIES=$LIB_FOLDER \
				-DZLIB_INCLUDE_DIRS=$INC_FOLDER -DZLIB_LIBRARIES=$LIB_FOLDER \
				-DLIBLZMA_INCLUDE_DIRS=$INC_FOLDER -DLIBLZMA_LIBRARIES=$LIB_FOLDER 
make -j8 install
cmake $SRC_TIFF -DCMAKE_INSTALL_PREFIX=$INSTALL_FOLDER -DCMAKE_BUILD_TYPE=release -DBUILD_SHARED_LIBS=no \
                -DJPEG_INCLUDE_DIR=$INC_FOLDER -DJPEG_LIBRARIES=$LIB_FOLDER \
                -DJPEG_INCLUDE_DIR=$INC_FOLDER -DJPEG_LIBRARIES=$LIB_FOLDER \
				-DZLIB_INCLUDE_DIRS=$INC_FOLDER -DZLIB_LIBRARIES=$LIB_FOLDER \
				-DLIBLZMA_INCLUDE_DIRS=$INC_FOLDER -DLIBLZMA_LIBRARIES=$LIB_FOLDER 
make -j8 install
clean_build_folder

# Build lcms
$SRC_LCMS/configure --prefix=$INSTALL_FOLDER --with-jpeg=$INSTALL_FOLDER --with-tiff=$INSTALL_FOLDER
make -j8 install
clean_build_folder

# Build jpeg2
cmake $SRC_OPENJPEG2 -DCMAKE_INSTALL_PREFIX=$INSTALL_FOLDER -DCMAKE_BUILD_TYPE=release \
                     -DPNG_PNG_INCLUDE_DIR=$INC_FOLDER -DPNG_LIBRARY=$LIB_FOLDER/libpng.a \
					 -DTIFF_INCLUDE_DIR=$INC_FOLDER -DTIFF_LIBRARY=$LIB_FOLDER/libtiff.a \
                     -DLCMS2_INCLUDE_DIR=$INC_FOLDER -DLCMS2_LIBRARY=$LIB_FOLDER/liblcms2.a \
					 -DZLIB_INCLUDE_DIR=$INC_FOLDER -DZLIB_LIBRARY_RELEASE=$LIB_FOLDER/libz.a \
					 -DBUILD_CODEC=no -DBUILD_SHARED_LIBS=no
make -j8 install
clean_build_folder

# Build ImageMagick
$SRC_IM/configure --prefix=$INSTALL_FOLDER --with-bzlib=no --disable-hdri --with-quantum-depth=16 \
                  PKG_CONFIG=$INSTALL_FOLDER/bin/pkg-config PKG_CONFIG_PATH=$LIB_FOLDER/pkgconfig
make -j8 install
clean_build_folder
